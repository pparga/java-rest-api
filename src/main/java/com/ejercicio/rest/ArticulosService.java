package com.ejercicio.rest;
 
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import entities.Articulo;

@Path("/articulos")
public class ArticulosService {

    @GET
    @Path("/")
    public Response getAllArticulos() {
        List<Articulo> articulos = new ArrayList<Articulo>();
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ejercicio-rest", "root", "admin");
            Statement stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM articulos";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){

                String id  = rs.getString("id");
                String nombre = rs.getString("nombre");
                String descripcion = rs.getString("descripcion");
                double precio = rs.getDouble("precio");
                String modelo = rs.getString("modelo");

                articulos.add(new Articulo(id, nombre, descripcion, precio, modelo));
            }


        } catch (SQLException e) {
            throw new IllegalStateException("Cannot connect the database!", e);
        }
        return  Response.status(200).entity(articulos).build();
    }
}
